#!/bin/bash 

APOPHEOZ="@apopheoz-support:matrix.org"
PHEIX="@pheix-support:matrix.org"
PASSWORD="$1"

PATH=/home/kostas/apache_logs/pheix.org.error.log
PATHPHEIX=/home/kostas/apache_root/pheix.org/www/conf/system/logs.tnk
HASHES=/home/kostas/utils/assets/processed.hashes
PRODROOM=%21rgxuOfUrUZgvtxJKME%3Amatrix.org
TESTROOM=%21XderpBcLtExhSrcXxx%3Amatrix.org

ROOM=$PRODROOM

line_processing () {
	local LINE="$1"
	local DATE="$2"
	local BODY=

	if ! [ -z "${DATE}" ]; then
		BODY="<b><font color=red>${DATE}</font>:</b><div><code>${LINE}</code></div>"
	else
		BODY="<div><code>${LINE}</code></div>"
	fi

	local HASH=`echo -n ${LINE} | /usr/bin/md5sum | /usr/bin/awk '{ print $1 }'`
	local UUID=`/usr/bin/uuidgen`
	local PROCESSED=

	if [ -f "${HASHES}" ]; then
		PROCESSED=`/bin/cat ${HASHES}`
	fi

	if ! [ -z "${PROCESSED}" ]; then
		if [[ $PROCESSED == *"${HASH}"* ]]; then
			return 1;
		else
			/usr/bin/curl -X PUT "https://matrix.org/_matrix/client/r0/rooms/${ROOM}/send/m.room.message/${UUID}?access_token=${TOKEN}" -H "Content-Type: application/json" -d "{\"msgtype\": \"m.text\",\"body\":\"\",\"format\":\"org.matrix.custom.html\",\"formatted_body\":\"${BODY}\"}"
			echo ${HASH} >> ${HASHES};
		fi;
	else
		/usr/bin/curl -X PUT "https://matrix.org/_matrix/client/r0/rooms/${ROOM}/send/m.room.message/${UUID}?access_token=${TOKEN}" -H "Content-Type: application/json" -d "{\"msgtype\": \"m.text\",\"body\":\"\",\"format\":\"org.matrix.custom.html\",\"formatted_body\":\"${BODY}\"}"

		echo ${HASH} >> ${HASHES}
		# echo && echo "Blank processed file, new hash ${HASH} is added"
	fi;

	return 0;
}

TOKEN=`/usr/bin/curl -sk -X POST -d "{\"type\":\"m.login.password\", \"user\":\"${APOPHEOZ}\", \"password\":\"${PASSWORD}\"}" https://matrix.org/_matrix/client/r0/login | /usr/bin/jq '. | select(has("access_token")) | .access_token' | /usr/bin/tr -d '"'`

if [ -z "$TOKEN" ]; then
	echo "can not obtain token for ${APOPHEOZ}"
	exit 1;
fi;

if [ -f "${PATH}" ]; then
	while IFS="" read -r p || [ -n "$p" ]
	do
		LINE=${p//[\']/'&apos;'}
		LINE=${LINE//[\"]/'&quot;'}
		LINE=${LINE//\(/'&lpar;'}
		LINE=${LINE//\)/'&rpar;'}
		LINE=${LINE//\//'&sol;'}
		LINE=${LINE//\</'&lt;'}
		LINE=${LINE//\>/'&gt;'}

		line_processing "${LINE}"

		DID=$?
	
		if [ $DID -eq 1 ]; then
			HASH=`echo -n $LINE | /usr/bin/md5sum | /usr/bin/awk '{ print $1 }'`
			echo "${HASH} already processed";
		else
			echo && echo "New hash! Processing ${HASH}"
			break
		fi
	done < ${PATH}
else
	echo "${PATH} not found"
fi

RES=`/usr/bin/curl -sk -X POST https://matrix.org/_matrix/client/r0/logout?access_token=${TOKEN}`

TOKEN=`/usr/bin/curl -sk -X POST -d "{\"type\":\"m.login.password\", \"user\":\"${PHEIX}\", \"password\":\"${PASSWORD}\"}" https://matrix.org/_matrix/client/r0/login | /usr/bin/jq '. | select(has("access_token")) | .access_token' | /usr/bin/tr -d '"'`

if [ -z "$TOKEN" ]; then
	echo "can not obtain token for ${PHEIX}"
	exit 2;
fi;

if [ -f "${PATHPHEIX}" ]; then 
	while IFS="" read -r p || [ -n "$p" ]
	do
		DATE=`echo ${p} | /bin/sed -r 's/^([0-9]+)\|.*$/\1/'` 	
		LINE=`echo ${p} | /bin/sed 's/^[0-9\|]\+//' | /usr/bin/base64 --decode | /usr/bin/tr -d '\n'`

		DATE=`/bin/date -d "@$DATE"`

		if [ -z "${DATE}" ]; then
			DATE="undefined"
		fi

		LINE=${LINE//[\']/'&apos;'}
		LINE=${LINE//[\"]/'&quot;'}
		LINE=${LINE//\(/'&lpar;'}
		LINE=${LINE//\)/'&rpar;'}
		LINE=${LINE//\//'&sol;'}	
		LINE=${LINE//\</'&lt;'}
		LINE=${LINE//\>/'&gt;'}

		line_processing "${LINE}" "${DATE}"

		DID=$?
	
		if [ $DID -eq 1 ]; then
			HASH=`echo -n $LINE | /usr/bin/md5sum | /usr/bin/awk '{ print $1 }'`
			echo "${HASH} already processed";
		else
			echo && echo "New hash! Processing ${HASH}"
			break
		fi	
	done < ${PATHPHEIX}
else
	echo "${PATHPHEIX} not found"
fi

exit 0;
